
## Why is there an animal welfare scale?

[There's reason to believe that a large number of non-human animals are
sentient](https://fcmconference.org/img/CambridgeDeclarationOnConsciousness.pdf). [An
estimated 170 billion animals are farmed and killed for human consumption each
year](https://forum.effectivealtruism.org/posts/pT7AYJdaRp6ZdYfny/estimates-of-global-captive-vertebrate-numbers)
while only about 107 billion people have ever lived. To decrease the price of
animal products factory farming practices have been optimised at the cost of
animal welfare. This has lead to a large number of farmed animals being
subjected to immense suffering. It is reasonable to consider changing our
dietary habits to reduce demand for products that cause such suffering.

Michael Huemer's [_Ethical
Vegetarianism_](https://digitalcommons.calpoly.edu/cgi/viewcontent.cgi?article=2174&context=bts)
is a short book discussing the ethics of consuming animal products.

## Are shrimp sentient? What are the harms associated with shrimp farming?

There have not been that many studies investigating whether decapod crustaceans
(an order which includes shrimp) are sentient but for some species [substantial
evidence of sentience
exists](https://www.lse.ac.uk/News/News-Assets/PDFs/2021/Sentience-in-Cephalopod-Molluscs-and-Decapod-Crustaceans-Final-Report-November-2021.pdf). Shrimp
are farmed in [greater
numbers](http://fishcount.org.uk/studydatascreens2/2017/numbers-of-farmed-decapods-A0-2017.php?sort2/full)
than all farmed land animals combined. In order to not risk causing a great
deal of harm it is reasonable to assume that decapod crustaceans are sentient
when making decisions about our diet.

Some of the [welfare issues of shrimp
aquaculture](https://www.shrimpwelfareproject.org/shrimp-welfare-report)
include eyestalk ablation which is the removal of eyestalks to increase
fertility of female shrimp, spread of diseases, high stocking density and poor
water quality such as insufficient level of dissolved oxygen.

Shrimp aquaculture is also [not
sustainable](https://www.shrimpwelfareproject.org/shrimp-welfare-and-sustainability). Shrimp
farming contributes to destruction of mangroves, viral disease outbreaks which
affect wild animals and releasing carbon dioxide from the seafloor by using
bottom trawling to catch fish who are used to produce shrimp feed.

## Why does milk have such a low score?

The [average annual milk yield of a US dairy cow is over 10,000
kg](https://usda.library.cornell.edu/concern/publications/h989r321c?locale=en). In
comparison [slaughtering a broiler yields around 2 kg of
meat](https://www.rspca.org.uk/webContent/staticImages/BroilerCampaign/EatSitSufferRepeat.pdf). More
small animals have to be raised and killed to produce the same amount of
calories as from killing a larger animal. This effect is especially pronounced
when comparing produce from smaller animals to milk.

## Why do eggs have such a high score?

Eggs have a high harm score on the welfare scale due to the opposite effect of
what causes the low score of milk. Chickens are small animals which means a
relatively large number of chickens have to be used to produce the amount of
eggs humans consume. Modern egg-laying hen breeds [can lay over 300 eggs a
year](https://extension.psu.edu/modern-egg-industry). Americans, for example,
eat [more than 200 eggs per person in a year]( are estimated to).

Another factor influencing the score of eggs is the choice of cumulative
elasticity factors. In the Animal Charity Evaluators' set of factors (the
default option in this tool) eggs have a high cumulative elasticity
factor. This assumes that decreased consumer demand for eggs has a smaller
effect on production than with other animal products.

Another noticeable difference is the high default suffering intensity factor of
caged hens. Although these scores are very subjective the default values are
based on what Brian Tomasik knew about the conditions of farmed
animals. Egg-laying hens are one of the worst off species used by
humans. Common farming practices include [culling male chicks by grinding them
alive](https://www.youtube.com/watch?v=HN0g13kMk6s), [debeaking chicks without
anaesthesia](https://www.youtube.com/watch?v=_6qCtjrveig) and keeping chickens
in small cages where the stress causes them to turn to cannibalism.

## Does the model consider breeder animals, fish meal, male chicks and male calves?

Although the author acknowledges that the number of these animals is
significant, due to the difficulty of obtaining this data for all species
included in the tool it was decided not to account for secondary deaths.

Here "secondary deaths" means the animals killed during the processes involved
in factory farming whose meat/eggs/milk does not reach store shelves. For
example male chicks of egg-laying hen breeds are killed shortly after birth
because they do not lay eggs and are not economical to raise for meat. Another
example would be fish farms where wild-caught fish are fed to carnivorous
species raised on the farms.
