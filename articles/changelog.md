# Changelog

## 28th of August 2023

- Use [USDA FoodData Central](https://fdc.nal.usda.gov/) data for the amount of
  produce required for 2000 kcal of energy. The
  [nutritiondata.self.com](http://nutritiondata.self.com) website used
  previously no longer exists.

## 11th of May 2023

- Replaced brain size/neuron count parameter with [Rethink Priorities' welfare
  range
  estimates](https://rethinkpriorities.org/publications/welfare-range-estimates).
- Removed suffering intensity parameter as it doesn't fit into the welfare
  range framework and is not based on empirical evidence.

Big thank you to Jeroen Borghuis for advice regarding welfare ranges.

## 14th of February 2023

- Made chart bars orange because green is considered a positive colour.

## 5th of February 2023

- Added FAQ page.
- Removed cage-free hens and slow-growing broilers because the only difference
  to caged hens and fast-growing broilers was the suffering intensity
  parameter. The default values of this parameter were not based on empirical
  evidence so this was considered misleading.

## 20th of January 2021

First version launched.
