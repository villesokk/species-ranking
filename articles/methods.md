---
title: "Tool for ranking farmed animal species when adopting a reducetarian diet"
---

## Motivation

In recent decades there has been increased coverage of animal welfare
issues, the health risks of high consumption of animal products and
the contribution of farming animals to climate change. Multiple
high-profile organisations have called for reduced animal consumption
through reducetarian, vegetarian and vegan diets [@ipccclimateland;
@willett2019food].

Each of these three issues has been studied extensively and solving
these issues has gained broad support. A balanced vegan diet has the
opportunity to reduce the negative impacts of all three issues but
campaigning for veganism has not been very successful and veganism
still has relatively low prevalence of under 10%. For example, in the
United Kingdom, where the first vegan society was founded, prevalence
of veganism is estimated at 1.16% by the most recent survey
[@vegansociety].

An alternative to veganism called flexitarianism or reducetarianism
has been gaining ground. The idea of these diets is to reduce
consumption of animal products by partially replacing them with
plant-based foods. If one were to adopt a flexitarian diet it would be
confusing to decide which species to avoid since consumption of
different species have different levels of associated harm on the
three scales. For example, poultry has lower health risks and
contributes less to climate change than beef but is considered a poor
choice from an animal welfare perspective due to the poor conditions
of the animals and the high number of animals used.

The goal of this tool is to allow the user to specify their relative
concern on two issues: animal welfare and climate change. The tool
ranks animal species according to the harm induced by their
consumption while taking into accord the user's values. This ranking
could be used to decide which animal products should be replaced with
plant-based products.

Rankings based on welfare have been developed previously by various
individuals and groups such as [Peter
Hurford](http://everydayutilitarian.com/essays/how-much-suffering-is-in-the-standard-american-diet/),
[Brian
Tomasik](https://reducing-suffering.org/how-much-direct-suffering-is-caused-by-various-animal-foods/),
[Charity
Entrepreneurship](https://forum.effectivealtruism.org/posts/g57AjP4HqTmfFTAde/from-humans-in-canada-to-battery-caged-chickens-in-the)
and [Dominik Peters](http://ethical.diet). This tool is a minor
extension of the work of Dominik Peters that also considers emissions
in addition to welfare. I want to thank Dominik for kindly providing
[the
data](https://docs.google.com/spreadsheets/d/1NF8he-Y1T3xhvjfqYsEmPk5eSCDWl-V4qUpHMZozjLQ/)
and methodology that he used.

## Animal suffering subscale

To estimate the negative impact on animal welfare, the tool calculates the
number of hours animals have spent in a farm in order to produce an amount of
food which provides 2000 kcal of energy. For example, Dominik retrieved data on
production yields and slaughter age from providers of breed chickens such as
Aviagen and Lohmann Tierzucht.

I calculated the amount of produce required for 2000 kcal of energy using data
from the [FoodData Central website](https://fdc.nal.usda.gov/) of the US
Department of Agriculture. Of course different preparations from the same
animal can have varying nutritional value but for the sake of simplicity one
value is used per species.

Let $suffering_s$ designate the suffering subscale score of species $s$. Let
$lifespan_s,\ production_s,\ refweight_s$ designate the average lifespan of an
animal in hours, weight of produce per animal and weight of produce required
for 2000 kcal of energy. The basic score is the number of hours suffered on a
farm to produce 2000 kcal of produce:

$$ suffering_s = \frac{lifespan_s}{production_s} \times refweight_s $$

There are some issues with this basic approach. For example, our confidence in
different species being sentient varies. If we just account for hours lived to
produce 2000 kcal then the harm of consuming smaller species will dominate. But
the user of the tool might have low confidence in shrimp being sentient and we
want to account for that.

Estimates of welfare range were used to account for differences in sentience
and capacity for welfare [@welfarerange]. The welfare range of an animal is the
range between the maximum positive welfare and minimum negative welfare the
animal can experience. To estimate welfare ranges Rethink Priorities collected
data on dozens of empirical traits related to the evolutionary functions of
pleasure and pain. Trait data of each species was aggregated into a value
between 0 and 1 where 1 corresponds to the welfare range of humans. These
estimates were corrected for the probability of sentience. The user can choose
to factor in welfare ranges in which case $suffering_s$ is multiplied by the
species' welfare range.

Different animal products might have different price
elasticities. Price elasticity of supply shows the responsiveness of
production to change in price. Elasticity of demand shows the
responsiveness of demand to change in price. Cumulative elasticity is
the net effect on supply. If someone spares 10 chickens a year by not
eating chickens the actual change could be less than 10. Decreased
chicken meat price due to lower demand might motivate someone else to
eat more chicken. The user can choose to factor in cumulative
elasticity in order to account for this effect. Two sources are
provided: the book "Compassion, by the Pound"
[@norwood_compassion_2011] and the work of the organisation Animal
Charity Evaluators [@ace].

The user can also choose to factor in sleeping time if they assume
that animals do not suffer while sleeping and liveability which also
factors in animals who die before slaughter.

## Climate change subscale

The climate change subscale measures the CO~2~ equivalent greenhouse
gases produced per kilogram of animal produce. This value is scaled
according to the amount of produce required for 2000 kcal. CO~2~
equivalent emissions data has been collected from lifecycle analyses
[@hamerschlag2011meat;@cao2011life]. Let $emissions_s$ designate the
CO~2~ equivalent gases produced per kilogram of produce of species
$s$. The climate subscale score of species $s$ is thus:

$$ climate_s = emissions_s \times refweight_s $$

The elasticity parameters apply both to the suffering and climate
subscales. That means if elasticity is enabled then $climate_s$ is
multiplied by the cumulative elasticity factor.

Note that only the impact on climate change is considered. There are
other negative environmental impacts. Saltwater fishing causes marine
pollution and fish farms cause eutrophication. But since the risk of
climate change outweighs other environmental risks related to the
consumption of animal produce I consider these omissions acceptable.

It is sometimes argued that buying local food is more important than
reducing meat consumption. In general the climate impact of food is
dominated by production [@weber2008food] so this tool does not make a
distinction between where animals were farmed. Imported plant-based
food tends to have lower emissions than local animal produce. Life
cycle analysis of animal products already includes transportation and
this is considered sufficient.

The tool also does not consider which plant-based foods are
substituted for animal products. Plant-based food production in
general causes significantly lower emissions [@poore2018reducing;
@Springmann4146] but from the perspective of the environment it might
make sense to prefer whole foods because these do not require
additional energy-intensive processing.

An issue with using CO~2~ equivalent greenhouse gas emissions as a
measure of warming is that farming different species puts different
types of greenhouse gases in the environment. The high impact of
ruminants is caused by their methane emissions. While methane warms
the atmosphere more than CO~2~ it is also removed from the atmosphere
significantly faster. Climate scientists sometimes use a metric called
CO~2~-equivalent with Global Warming Potential 100 which considers
methane to cause 25x as much warming as an equivalent amount of CO~2~
over a century. Some physicists disagree with this approach
[@allen2015short]. If one is concerned about the effects of warming
over thousands of years as opposed to a hundred years this approach
understates the impact of CO~2~ compared to methane.

## Combined model

A [weighted product
model](https://en.wikipedia.org/wiki/Weighted_product_model) is used
to combine the subscales. Weighted product models are dimensionless
and are used for ranking options when making decisions. Because the
scores are dimensionless they are normalised to the range $[0, 100]$.

Let $w_{suffering}$ and $w_{climate}$ designate the suffering and
climate weights. The combined score of species $s$ is calculated
using:

$$ harm_s = suffering_s^{w_{suffering}} \cdot climate_s^{w_{climate}}
$$

A product model ensures that the subscales affect the combined score
equivalently. A 1% increase in CO~2~ emissions changes the combined
score by the same amount that a 1% increase in the animal suffering
subscale would. Adding weights to the model allows us to change the
relative contribution of each subscale to the combined score.

## Why is there no health impacts subscale?

I considered designing a health subscale using data from the [Global
Burden of Disease (GBD)](http://www.healthdata.org/gbd) study but
eventually opted against it.

Understanding nutrition is notoriously difficult. It is impossible to
conduct trials that assess long-term health impacts of diets due to
costs and ethical concerns with assigning people to diets with unknown
health effects. Due to this dietary decisions must be made based
mostly on observational data which is not as reliable as randomised
controlled trials. Even well studied questions such as the impact of
saturated fat consumption have not been fully resolved
[@zhu2019dietary; @hooper2020reduction].

GBD data has been aggregated from a large number of sources by a large
team. It might be close to the consensus of nutritional science but
even if we have reasons to trust the data it would be difficult to use
due to non-linear effects. If two people decided to eat one less
chicken this year their climate and animal welfare impact would be
similar even if one of them previously ate 10 chickens per year and
the other ate a single chicken each year. Positive health effects of
reduced meat consumption on the other hand have diminishing returns
and the reduction of their health risks would depend on the current
composition of their diets.

Adequately planned reducetarian, vegetarian and vegan diets are
believed to be healthy based on existing evidence [@craig2009position]
but positive effects over conventional diets are not considered in
this tool due the uncertainty of the effects and difficulties of
modeling.

## Limitations

Multiple issues with the method used to estimate suffering are
outlined in [@browning2020]. The tool could be helpful to make
decisions in the face of uncertainty but is not a true measure of
harm.

The model does not consider the suffering of wild animals which could
significantly exceed that of farm animals
[@tomasik_importance_2015]. There is also no consideration of which
plant-based products are substituted for animal products. Farming of
some plants causes less emissions [@poore2018reducing] or wild animal
suffering [@tomasikcropcultivation].

I consider the greatest limitation of the tool the fact that setting
subscale priorities based on intuitions can be misleading. It would
make sense to compare emissions and harm based on the underlying
values which cause us to be concerned about the issues in the first
place. If for example I am motivated by increased welfare, it would be
helpful to estimate the welfare impacts of climate change and factory
farming on a common scale.

## Sources

------------------------------ ------------------------------
**Parameter**                  **Source**

Lifespan                       [Dominik Peters](https://docs.google.com/spreadsheets/d/1NF8he-Y1T3xhvjfqYsEmPk5eSCDWl-V4qUpHMZozjLQ/)

Production                     [Dominik Peters](https://docs.google.com/spreadsheets/d/1NF8he-Y1T3xhvjfqYsEmPk5eSCDWl-V4qUpHMZozjLQ/)

Sleeping time                  [Dominik Peters](https://docs.google.com/spreadsheets/d/1NF8he-Y1T3xhvjfqYsEmPk5eSCDWl-V4qUpHMZozjLQ/)

Pre-slaughter mortality        [Dominik Peters](https://docs.google.com/spreadsheets/d/1NF8he-Y1T3xhvjfqYsEmPk5eSCDWl-V4qUpHMZozjLQ/)

Welfare range                  Rethink Priorities [@welfarerange]

Elasticity factors             Animal Charity Evaluators [@ace];
                               Compassion, by the pound [@norwood_compassion_2011]

Emissions                      [Dominik Peters](https://docs.google.com/spreadsheets/d/1NF8he-Y1T3xhvjfqYsEmPk5eSCDWl-V4qUpHMZozjLQ/); Cao et al. [@cao2011life]
------------------------------ ------------------------------

### Food energy

------------------------------ ------------------------------
**Animal**                     **Product**

Laying hen                     [Boiled egg](https://fdc.nal.usda.gov/fdc-app.html#/food-details/173424/nutrients)

Broiler                        [Cooked breast](https://fdc.nal.usda.gov/fdc-app.html#/food-details/171477/nutrients)

Pig                            [Cooked ground pork](https://fdc.nal.usda.gov/fdc-app.html#/food-details/167903/nutrients)

Turkey                         [Cooked meat](https://fdc.nal.usda.gov/fdc-app.html#/food-details/171479/nutrients)

Beef cow                       [Cooked ground beef](https://fdc.nal.usda.gov/fdc-app.html#/food-details/172161/nutrients)

Dairy cow                      [Whole milk](https://fdc.nal.usda.gov/fdc-app.html#/food-details/171265/nutrients)

Lamb                           [Cooked ground lamb](https://fdc.nal.usda.gov/fdc-app.html#/food-details/172544/nutrients)

Salmon                         [Cooked salmon](https://fdc.nal.usda.gov/fdc-app.html#/food-details/175168/nutrients)

Duck                           [Cooked duck](https://fdc.nal.usda.gov/fdc-app.html#/food-details/172411/nutrients)

Shrimp                         [Cooked crustaceans, shrimp](https://fdc.nal.usda.gov/fdc-app.html#/food-details/175180/nutrients)
------------------------------ ------------------------------

## References
