const _ = require('lodash')
const execFileSync = require('child_process').execFileSync
const fs = require('fs')
const jsdom = require('jsdom')
const katex = require('katex')
const path = require('path')
const { JSDOM } = jsdom

function translateToHTML(filename) {
  return execFileSync('pandoc', [
    '-f', 'markdown+smart+tex_math_dollars+subscript+superscript',
    '-t', 'html',
    '--citeproc',
    '--bibliography', './bibliography.bib',
    '--metadata', 'link-citations:true',
    '--csl', './citation-style.csl',
    '--katex',
    filename
  ], {
    cwd: '../articles/'
  }).toString('utf-8')
}

function renderMath(dom) {
  const equations = dom.window.document.querySelectorAll('span.math')

  for (const eqn of equations) {
    eqn.innerHTML = katex.renderToString(eqn.textContent)
  }
}

function processArticle(filename) {
  const html = translateToHTML(filename)
  const dom = new JSDOM(html)

  renderMath(dom)

  let serialized = ''
  for (const el of dom.window.document.body.children) {
    serialized += el.outerHTML
  }

  const outputName = path.parse(filename).name + '.html'

  const fd = fs.openSync('../articles/' + outputName, 'w')
  fs.writeFileSync(fd, serialized)
}

async function main() {
  if (process.argv.length !== 3) {
    throw new Error('Expecting markdown file name.')
  }

  const filename = process.argv[2]

  if (!fs.existsSync('../articles/' + filename)) {
    throw new Error(`File ${filename} does not exist.`)
  }

  processArticle(filename)
}

main()
