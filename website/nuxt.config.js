
const title = 'Food impacts';
const description = 'Which animal products should we avoid? A tool to help us decide.';

const accentColor = '#53c982';

export default {
  mode: 'universal',

  env: {
    accentColor
  },

  /*
  ** Headers of the page
  */
  head: {
    title: title,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: description },
      { hid: 'og:title', property: 'og:title', content: title },
      { hid: 'og:description', property: 'og:description', content: description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png?5' },
      { rel: 'stylesheet', href: "https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,300;0,700;1,300&display=swap" }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: accentColor },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/fontawesome.js',
    '~/plugins/shynet.js'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  /*
  ** Build configuration
  */
  build: {
    postcss: {
      plugins: {
        'precss': {
          variables: {
            accentColor,
            bpPhone: '576px',
            bpTablet: '768px'
          }
        },
        'lost': {},
        'postcss-calc': {},
        'postcss-color-function': {},
        'autoprefixer': {},
        'postcss-clean': {}
      }
    },

    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      config.module.rules.push({
        test: /\.html$/,
        loader: 'raw-loader'
      });
    }
  },

  /*
   * Vue configuration
   */
  vue: {
    config: {
      chainWebpack: function(webpackConfig) {
        webpackConfig.module.rules.delete("pug")

        webpackConfig.module
          .rule("pug")
            .test(/\.pug$/)

            .oneOf("vue-loader")
              .resourceQuery(/^\?vue/)
              .use("pug-plain")
                .loader("pug-plain-loader")
                .end()
            .end()

            .oneOf("raw-pug-files")
              .use("pug-html")
                .loader("html-loader")
                .end()
              .use("pug-plain")
                .loader("pug-plain-loader")
                .end()
            .end()
      }
    }
  },

  router: {
    linkExactActiveClass: 'active'
  }
}
