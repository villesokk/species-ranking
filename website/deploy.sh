#!/bin/sh

set -e

echo "Building"
export NODE_ENV=production
npm run generate

echo "Compressing"
cd dist
tar cf ../dist.tar.gz *
cd ..
echo "Uploading"
scp dist.tar.gz nooch.eu:~
echo "Removing old site"
ssh nooch.eu rm -fr /var/www/foodimpacts/*
echo "Unpacking new site"
ssh nooch.eu tar xf ~/dist.tar.gz -C /var/www/foodimpacts
echo "Removing .tar.gz"
ssh nooch.eu rm -f ~/dist.tar.gz
rm dist.tar.gz
echo "Done"
