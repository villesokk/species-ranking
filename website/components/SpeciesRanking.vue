<template lang="pug">
#species-ranking
  .columns
    .plot-col
      .plot-container(ref="plot" :class="{ desktop: isDesktop }")
        div
          h2.mt-0.mb-0.d-inline Ranking
          a.button.save(@click="savePlotAsPNG")
            font-awesome-icon.icon(icon="download")
            | .png
          a.button.save(@click="savePlotAsSVG")
            font-awesome-icon.icon(icon="download")
            | .svg
          .note Higher scores indicate greater harm.
        svg.plot(preserveAspectRatio="xMinYMin meet" :viewBox="viewBox")

    .parameters-col
      h2.mt-0.mb-2 Which animal products should we avoid?

      .note.
        People are becoming increasingly aware of the negative impacts
        of high consumption of meat, dairy and eggs to climate change
        and the welfare of farm animals. Reducetarian or flexitarian
        diets aim to replace some of these products with plant-based
        foods. This tool allows you to set your priorities on these
        two issues in order to rank consumption of different animal
        species according to contributions to climate change and
        farmed animal suffering. <nuxt-link to="/methods">You can read
        about how this tool works.</nuxt-link>

      h3 Priorities
      .note
        .weights-warning(v-if="weightsInvalid")
          font-awesome-icon.icon(icon="exclamation-triangle")
          | Weights do not sum to 100%
        span(v-else).
          Set your relative priorities of animal welfare and climate change.

      .sliders
        .slider-row(v-for="[weight, dict] in Object.entries(mp.weights)")
          .label-col {{ dict.label }}
          .slider-col
            .slider-container
              input.slider(
                type="range" min="1" max="99"
                :id="'slider-' + weight"
                @input="sliderChange(weight, $event)"
                :disabled="dict.locked")
            span.fraction {{ Math.round(dict.weight) }}%

      h3 Welfare range
      .note
        p.
          Different animals may be able to suffer or experience pleasure with a
          different intensity. For example, breaking a body part or being
          confined may feel worse for a pig than for a shrimp. An animal's
          welfare range is the range between their maximum positive welfare and
          minimum negative welfare. The most elaborate research to date
          provides estimates of welfare ranges which are based on animals’
          scoring on dozens of traits related to the evolutionary functions of
          pleasure and pain. The estimates are relative to humans, who score
          1. The estimates are corrected for the probability of sentience
          (i.e., the different likelihoods that these species may feel anything
          at all).

        p.
          No estimates are available for turkeys, ducks, lambs and
          cows. Therefore, we used the estimates of their closest relatives for
          whom estimates are available (chickens and pigs).

      .checkbox
        input#consider-welfare-range(type="checkbox" v-model="mp.considerWelfareRange")
        label(for="consider-welfare-range") Consider welfare range
      .welfare-range-parameters.toggleable(:class="{ disabled: !mp.considerWelfareRange }")
        span Relative welfare range:
        .boxes
          .box(v-for="species in mp.speciesData")
            .label {{ species.label }}
            .input
              input(
                type="number"
                v-model="species.welfareRange"
                min="0"
                max="1"
                step="0.1"
                :disabled="!mp.considerWelfareRange"
              )

      h3 Supply and demand elasticity
      .note.
        Price elasticity of production (or demand) shows the
        responsiveness of production (or demand) to change in
        price. If someone spares 10 chickens a year by not eating
        chickens the actual change could be less than 10. Decreased
        chicken meat price due to lower demand might motivate someone
        else to eat more chicken. You can factor in elasticities to
        account for this.

      .checkbox
        input#elasticity-toggle(type="checkbox" v-model="mp.considerElasticity")
        label(for="elasticity-toggle") Consider elasticity
      .elasticy-parameters.toggleable(:class="{ disabled: !mp.considerElasticity }")
        span Source:
        .radio.form-field
          input#cbp-elasticity(
            type="radio" value="cbp" v-model="mp.elasticitySource"
            :disabled="!mp.considerElasticity")
          label(for="cbp-elasticity") Compassion, by the Pound
        .radio.form-field
          input#ace-elasticity(type="radio" value="ace"
            v-model="mp.elasticitySource"
            :disabled="!mp.considerElasticity")
          label(for="ace-elasticity") Animal Charity Evaluators

      h3 Species
      .note Choose which animals are displayed in the plot.
      .species-parameters
        .checkbox(v-for="[key, species] in Object.entries(mp.speciesData)")
          input(:id="key" type="checkbox" v-model="species.enabled")
          label(:for="key") {{ species.label }}

      h3 Other parameters

      .note.
        When including animals who die before slaughter their age
        is set to half the slaughter age due to missing data on
        age of death of these animals.

      .note.
        Whether fish sleep is an <a
        href="https://en.wikipedia.org/wiki/Sleep_in_fish"
        target="_blank">open question</a>. The sleeping time for
        salmon and shrimp is set to zero.

      .checkbox.form-field
        input#liveability(type="checkbox" v-model="mp.considerLiveability")
        label(for="liveability") Include animals who die before slaughter
      .checkbox.form-field
        input#exclude-sleep(type="checkbox" v-model="mp.excludeSleep")
        label(for="exclude-sleep") Exclude sleeping time
</template>

<script>
import TweenLite from 'gsap'
import * as d3 from 'd3'
import { saveSvgAsPng } from 'save-svg-as-png'
import { speciesData, scoreSpecies } from './species'

/* Used for d3 plot */
let xDomain
let yDomain

export default {
  name: 'SpeciesRankingTool',

  data() {
    return {
      /* Mnemonic: model parameters */
      mp: {
        speciesData,

        /* Moral weights */
        weights: {
          welfare: {
            label: 'Welfare',
            weight: 50,
            locked: false
          },
          climate: {
            label: 'Climate',
            weight: 50,
            locked: false
          }
        },

        /* Welfare range */
        considerWelfareRange: true,

        /* Species toggles */
        cagedHen: true,
        cageFreeHen: true,
        broiler: true,
        slowBroiler: true,
        pig: true,
        turkey: true,
        beefCow: true,
        dairyCow: true,
        lamb: true,
        salmon: true,
        duck: true,
        shrimp: true,

        /* Elasticity */
        considerElasticity: true,
        elasticitySource: 'ace',

        /* Other parameters */
        considerLiveability: false,
        excludeSleep: false
      },

      /* UI stuff */
      plotHeight: 0,
      accentColor: process.env.accentColor,
      barColor: '#ffb397',
      plotFont: "12px 'Fira Sans', 'Roboto', 'Helvetica', 'Arial', sans-serif"
    }
  },

  computed: {
    plotDimensions() {
      const winWidth = window.innerWidth
      let width = 0
      const leftMargin = 120
      const topMargin = 10
      const margin = 20

      if (winWidth <= 576) {
        width = 250
      } else if (winWidth <= 768) {
        // Actually greater than 1280 case because the layout switches to one
        // column
        width = 500
      } else if (winWidth <= 1280) {
        width = 400
      } else if (winWidth <= 1440) {
        width = 500
      } else {
        width = 600
      }

      const height = (this.isDesktop ? 0.6 : 1) * width

      return {
        width: Math.round(width),
        height: Math.round(height),
        margin,
        leftMargin,
        topMargin
      }
    },

    viewBox() {
      const { width, height } = this.plotDimensions
      return `0 0 ${width} ${height}`
    },

    isDesktop() {
      return window.innerWidth > 992
    },

    weightsInvalid() {
      let total = 0
      for (const [, dict] of Object.entries(this.mp.weights)) {
        total += dict.weight
      }
      return Math.round(total) !== 100
    }
  },

  watch: {
    mp: {
      handler() {
        this.updateRankingPlot()
      },
      deep: true
    }
  },

  mounted() {
    this.setupRankingPlot()

    for (const [w, dict] of Object.entries(this.mp.weights)) {
      document.querySelector('#slider-' + w).value = dict.weight
    }

    document.addEventListener('scroll', this.scrollRankingPlot)
  },

  beforeDestroy() {
    document.removeEventListener('scroll', this.scrollRankingPlot)
  },

  methods: {
    setupRankingPlot() {
      const small = this.plotDimensions.margin
      const margin = {
        top: this.plotDimensions.topMargin,
        right: small,
        bottom: small,
        left: this.plotDimensions.leftMargin
      }

      const width = this.plotDimensions.width - margin.left - margin.right
      const height = this.plotDimensions.height - margin.top - margin.bottom
      const data = this.score()

      /* Domains */
      yDomain = d3.scaleBand()
        .range([height, 0])
        .padding(0.2)

      xDomain = d3.scaleLinear()
        .range([0, width])

      xDomain.domain([0, d3.max(data, d => d.score)])
      yDomain.domain(data.map(d => d.species))

      /* Background */
      const svg = d3.select('#species-ranking .plot-container .plot')
      svg
        .append('rect')
        .attr('width', '100%')
        .attr('height', '100%')
        .style('fill', '#ffffff')

      /* Bars group */
      const bars = svg
        .append('g')
        .attr('class', 'bars')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

      /* Bars */
      bars.selectAll('.bar')
        .data(data)
        .enter().append('rect')
        .attr('class', 'bar')
        .attr('width', d => xDomain(d.score))
        .attr('y', d => yDomain(d.species))
        .attr('height', yDomain.bandwidth())
        .style('fill', this.barColor)

      /* Axises (how is it in plural :D?) */
      const xAxis = d3.axisBottom(xDomain).ticks(5)
      bars.append('g')
        .attr('transform', 'translate(0,' + height + ')')
        .attr('class', 'xaxis')
        .call(xAxis)

      const yAxis = d3.axisLeft(yDomain)
      bars.append('g')
        .attr('class', 'yaxis')
        .call(yAxis.tickSize(0))

      /* Font and axis line style */
      svg.selectAll('text')
        .style('fill', '#555')
        .style('font', this.plotFont)
        .style('font-weight', '400')
      svg.selectAll('.domain, .tick line').attr('stroke', '#555')
    },

    updateRankingPlot() {
      const data = this.score()
      const svg = d3.select('#species-ranking .plot-container .plot')

      /* Axis domains */
      xDomain.domain([0, d3.max(data, d => d.score)])
      const xAxis = d3.axisBottom(xDomain).ticks(5)
      svg.select('.xaxis').transition().call(xAxis)

      yDomain.domain(data.map(d => d.species))
      const yAxis = d3.axisLeft(yDomain)
      svg.select('.yaxis').transition().call(yAxis.tickSize(0))

      const bars = svg.selectAll('.bar').data(data)
      const t = d3.transition().duration(500)

      /* Update */
      bars
        .transition(t)
        .attr('width', d => xDomain(d.score))
        .attr('height', yDomain.bandwidth())
        .attr('y', d => yDomain(d.species))

      /* Enter */
      bars
        .enter()
        .select('.bars')
        .append('rect')
        .style('fill', this.barColor)
        .transition(t)
        .attr('class', 'bar')
        .attr('width', d => xDomain(d.score))
        .attr('y', d => yDomain(d.species))
        .attr('height', yDomain.bandwidth())

      /* Exit */
      bars
        .exit()
        .transition(t)
        .remove()

      /* Font and axis line style */
      svg.selectAll('text')
        .style('fill', '#555')
        .style('font', this.plotFont)
        .style('font-weight', '400')
      svg.selectAll('.domain, .tick line').attr('stroke', '#555')
    },

    scrollRankingPlot() {
      const scrollTop = window.pageYOffset || document.documentElement.scrollTop
      const that = this
      TweenLite.to(that, 0.3, {
        plotHeight: scrollTop,
        onUpdate: that.scrollRankingPlotUpdate
      })
    },

    scrollRankingPlotUpdate() {
      this.$refs.plot.style.top = this.plotHeight + 'px'
    },

    savePlotAsPNG() {
      saveSvgAsPng(
        document.querySelector('#species-ranking .plot-container .plot'),
        'species.png',
        { scale: 2 }
      )
    },

    savePlotAsSVG() {
      const svg = document.querySelector('#species-ranking .plot-container .plot').outerHTML
      const element = document.createElement('a')
      element.setAttribute('href', 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent(svg))
      element.setAttribute('download', 'species.svg')
      element.style.display = 'none'
      document.body.appendChild(element)
      element.click()
      document.body.removeChild(element)
    },

    sliderChange(weight, event) {
      const newWeight = Number(event.target.value)
      let total = newWeight
      for (const [other, dict] of Object.entries(this.mp.weights)) {
        if (other !== weight) {
          total += dict.weight
        }
      }

      const adjustment = 100 - total
      this.mp.weights[weight].weight = newWeight

      for (const [other, dict] of Object.entries(this.mp.weights)) {
        if (weight !== other) {
          const w = Math.min(
            99,
            Math.max(1, dict.weight + adjustment)
          )
          dict.weight = w
          document.querySelector('#slider-' + other).value = w
        }
      }
    },

    score() {
      const scores = scoreSpecies(this.mp)

      scores.sort((a, b) => d3.ascending(a.score, b.score))

      // Normalise to 0-100
      for (const s of scores) {
        s.score = s.score / scores[scores.length - 1].score * 100
      }

      return scores
    }
  }
}
</script>

<style>
@lost flexbox flex;

#species-ranking {
  display: flex;
  flex-direction: row;
  justify-content: center;

  @media (max-width: $bpPhone) {
    font-size: 13pt;
  }

  h1, h2 {
    font-weight: 900;
    margin-top: 40pt;
    margin-bottom: 20pt;
  }

  h3 {
    font-weight: 700;
  }

  .mt-0 {
    margin-top: 0;
  }

  .mb-0 {
    margin-bottom: 0;
  }

  .mt-2 {
    margin-top: 2em;
  }

  .d-inline {
    display: inline;
  }

  .columns {
    max-width: 2000px;
    lost-flex-container: row;

    @media (max-width: 2000px) {
      max-width: none;
    }

    .plot-col {
      lost-column: 1/2;
      @media (max-width: $bpTablet) {
        lost-column: 1/1;
      }
    }

    .parameters-col {
      lost-column: 1/2;
      @media (max-width: $bpTablet) {
        lost-column: 1/1;
      }
    }
  }

  .plot-container {
    .yaxis .domain {
      display: none;
    }

    .save {
      font-size: 1rem;
      margin-left: 1em;
      position: relative;
      bottom: 0.3rem;
      font-weight: 700;
    }

    .note {
      display: inline;
      font-weight: 400;
      font-size: 1rem;
      margin-left: 1em;
    }

    &.desktop {
      position: relative;
    }

    @media (max-width: $bpTablet) {
      .note {
        display: block;
        margin: 0.5em 0 0 0;
      }
    }
  }

  .parameters-col {
    .slider-row {
      lost-flex-container: row;
      margin-bottom: 1em;

      &:last-of-type {
        margin-bottom: 0;
      }

      .label-col {
        lost-column: 1/6;
      }

      .slider-col {
        lost-column: 5/6;
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;

        .slider-container {
          flex-grow: 1;
          flex-basis: 0;

          input {
            position: relative;
            top: 0.3em;

            @media (max-width: $bpPhone) {
              top: 0;
            }
          }
        }

        .fraction {
          flex-grow: 0;
          display: inline-block;
          width: 2.3em;
          min-width: 2.3em;
          font-weight: 500;
          padding-left: 0.5em;
        }

        .button {
          flex-grow: 0;
          flex-basis: 1.5em;
          display: inline-block;
          font-size: 0.7em;
          margin-left: 0.5em;
          padding: 5px;
          width: 1.5em;
          text-align: center;
        }

        .icon {
          margin: 0;
        }
      }
    }
  }

  .weights-warning {
    margin-top: 1em;

    .icon {
      color: #f6b131;
      margin-right: 0.5em;
    }
  }

  .brain-parameters,
  .elasticy-parameters,
  .welfare-range-parameters {
    margin-top: 1em;
  }

  .toggleable {
    transition: opacity 0.2s ease;
  }

  .species-parameters {
    .checkbox {
      display: inline-block;
      width: auto;
      margin-right: 1em;
      margin-bottom: .5em;
    }
  }

  .welfare-range-parameters {
    .boxes {
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      margin-top: 1em;

      .box {
        margin-bottom: 0.5em;
        width: 50%;

        @media (max-width: $bpPhone) {
          width: 100%;
        }
      }

      .label {
        display: inline-block;
        width: 70%;
      }

      .input {
        display: inline-block;
        width: 30%;

        input {
          text-align: center;
          width: 3em;
        }
      }
    }
  }

  .slider {
    appearance: none;
    background-color: #eee;
    height: 18px;
    position: relative;
    direction: ltr;
    touch-action: none;
    user-select: none;
    box-sizing: border-box;
    border-radius: 1rem;
    min-width: 100%;

    &::-moz-focus-outer {
      border: 0;
    }

    &::-webkit-slider-thumb {
      appearance: none;
      width: 30px;
      height: 30px;
      background-color: $accentColor;
      cursor: pointer;
      border-radius: 1rem;
      border-width: 0;
    }

    &::-moz-range-thumb {
      width: 30px;
      height: 30px;
      background-color: $accentColor;
      cursor: pointer;
      border-radius: 1rem;
      border-width: 0;
    }
  }

  .radio,
  .checkbox {
    position: relative;
  }

  .radio label,
  .radio input,
  .checkbox label,
  .checkbox input {
    cursor: pointer;
  }

  .checkbox > input:focus ~ label::before {
    box-shadow: 0 0 0 1px $accentColor;
    border-color: $accentColor;
  }

  .radio > label,
  .checkbox > label {
    display: inline;
    padding: 0 0 0 1em;
    -moz-user-select: none;
    user-select: none;
  }

  .radio > label::before,
  .checkbox > label::before {
    box-sizing: border-box;
  }

  .radio > label::before {
    font-size: 0.7em;
    line-height: 1.3em;
    text-align: center;
    vertical-align: middle;
    border-radius: 100%;
    content: " ";
    transition: border 0.2s ease-in-out;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.3);
    height: 1em;
    left: .125em;
    position: absolute;
    top: .3em;
    width: 1em;
  }

  .radio > input:checked ~ label::before {
    border: 5px solid $accentColor;
  }

  .radio > input:focus ~ label::before {
    border: 5px solid $accentColor;
    box-shadow: 0 0 4px 2px color($accentColor a(0.3));
  }

  .radio > input,
  .checkbox > input {
    appearance: none;
    background: none;
    opacity: .00001;
    z-index: 2;
  }

  .checkbox > label::before {
    font-size: 0.5em;
    line-height: 1.3em;
    text-align: center;
    vertical-align: middle;
    content: ' ';
    display: inline-block;
    border-radius: 5px;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.3);
    width: 1.5em;
    height: 1.5em;
    position: absolute;
    left: 0;
    top: 0.2rem;
    transition: background-color 0.2s ease, border-color 0.2s ease, box-shadow 0.2s ease;
  }

  .checkbox > input:checked ~ label::before {
    padding: 0.1em;
    content: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Cpath fill='rgb(255,255,255)' d='M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z'%3E%3C/path%3E%3C/svg%3E");
    color: #fff;
    background-color: $accentColor;
    border-color: $accentColor;
  }

  .form-field {
    margin-top: .3em;
  }

  input[type="number"] {
    border: 1px solid $accentColor;
    padding: .3em .5em .3em .5em;
    border-radius: 0.3em;
    color: #555;
    font-size: 0.8em;
    font-weight: 300;
    -moz-appearance: textfield;
    transition: box-shadow 0.2s ease, background-color 0.2s ease;

    &:focus {
      box-shadow: 0 0 4px 2px color($accentColor a(0.3));
    }

    &:disabled {
      background-color: #fff;
    }
  }

  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  .note {
    margin-bottom: 1.5em;
  }

  .disabled {
    opacity: 0.5;
  }

  .text-center {
    text-align: center;
  }
}
</style>
