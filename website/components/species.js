const speciesData = {
  'caged-hen': {
    label: 'Laying hen',
    foodLabel: 'Hen egg',
    enabled: true,
    lifespan: 504,
    production: 20.5,
    sleeping: 45.83,
    liveability: 100,
    elasticityACE: 0.91,
    elasticityCBP: 0.91,
    co2: 1.86,
    refWeight: 1.2903,
    welfareRange: 0.332
  },
  broiler: {
    label: 'Chicken',
    enabled: true,
    lifespan: 42,
    production: 1.83,
    sleeping: 16.67,
    liveability: 97,
    elasticityACE: 0.3,
    elasticityCBP: 0.76,
    co2: 2.33,
    refWeight: 1.2121,
    welfareRange: 0.332
  },
  pig: {
    label: 'Pig',
    enabled: true,
    lifespan: 183,
    production: 91.1,
    sleeping: 33.33,
    liveability: 100,
    elasticityACE: 0.57,
    elasticityCBP: 0.74,
    co2: 4.62,
    refWeight: 0.6734,
    welfareRange: 0.515
  },
  turkey: {
    label: 'Turkey',
    enabled: true,
    lifespan: 126,
    production: 10.3,
    sleeping: 33.33,
    liveability: 100,
    elasticityACE: 0.33,
    elasticityCBP: 0.76,
    co2: 3.41,
    refWeight: 1.0582,
    welfareRange: 0.332
  },
  'beef-cow': {
    label: 'Beef cow',
    enabled: true,
    lifespan: 402,
    production: 339,
    sleeping: 16.67,
    liveability: 100,
    elasticityACE: 0.5,
    elasticityCBP: 0.68,
    co2: 15.23,
    refWeight: 0.8333,
    welfareRange: 0.515
  },
  'dairy-cow': {
    label: 'Dairy cow',
    foodLabel: 'Milk',
    enabled: true,
    lifespan: 2009,
    production: 50420,
    sleeping: 16.67,
    liveability: 100,
    elasticityACE: 0.45,
    elasticityCBP: 0.56,
    co2: 1.062,
    refWeight: 3.2787,
    welfareRange: 0.515
  },
  lamb: {
    label: 'Lamb',
    enabled: true,
    lifespan: 180,
    production: 60,
    sleeping: 16.67,
    liveability: 100,
    elasticityACE: 0.5,
    elasticityCBP: 0.68,
    co2: 20.44,
    refWeight: 0.7067,
    welfareRange: 0.515
  },
  'farmed-salmon': {
    label: 'Farmed salmon',
    enabled: true,
    lifespan: 720,
    production: 2.73,
    sleeping: 0,
    liveability: 100,
    elasticityACE: 0.43,
    elasticityCBP: 0.43,
    co2: 4.14,
    refWeight: 0.9709,
    welfareRange: 0.056
  },
  duck: {
    label: 'Duck',
    enabled: true,
    lifespan: 45,
    production: 3.5,
    sleeping: 40,
    liveability: 95,
    elasticityACE: 0.3,
    elasticityCBP: 0.76,
    co2: 2.33,
    refWeight: 0.995,
    welfareRange: 0.332
  },
  shrimp: {
    label: 'Shrimp',
    enabled: true,
    lifespan: 180,
    production: 0.025,
    sleeping: 0,
    liveability: 70,
    elasticityACE: 0.43,
    elasticityCBP: 0.43,
    co2: 3.1,
    refWeight: 2.0202,
    welfareRange: 0.031
  }
}

/*
 * TODO: this must be improved
 */
const preSlaughterDeathAge = 0.5

/*
 * mp - model parameters. Currently you can look at the vue component
 * to see what parameters are expected and how they are named.
 */
function scoreSpecies(mp) {
  const welfareWeight = mp.weights.welfare.weight / 100
  const climateWeight = mp.weights.climate.weight / 100

  const scores = []

  for (const [, species] of Object.entries(mp.speciesData)) {
    if (!species.enabled) {
      // eslint-disable-next-line no-continue
      continue
    }

    let welfare = species.lifespan / species.production * 24 * species.refWeight
    let climate = species.co2 * species.refWeight

    if (mp.considerLiveability) {
      welfare *= ((1 / (species.liveability / 100) - 1) * preSlaughterDeathAge + 1)
    }

    if (mp.excludeSleep) {
      welfare *= (1 - species.sleeping / 100)
    }

    if (mp.considerElasticity) {
      if (mp.elasticitySource === 'ace') {
        welfare *= species.elasticityACE
        climate *= species.elasticityACE
      } else if (mp.elasticitySource === 'cbp') {
        welfare *= species.elasticityCBP
        climate *= species.elasticityCBP
      } else {
        throw new Error('Unknown elasticity source: ' + mp.elasticitySource)
      }
    }

    if (mp.considerWelfareRange) {
      welfare *= species.welfareRange
    }

    const score = (welfare ** welfareWeight) * (climate ** climateWeight)

    const label = species.foodLabel || species.label
    const dict = {
      species: label,
      score
    }

    scores.push(dict)
  }

  return scores
}

export {
  speciesData,
  scoreSpecies
}
