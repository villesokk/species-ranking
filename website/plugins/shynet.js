const Shynet = {
  idempotency: null,
  heartbeatTaskId: null,
  sendHeartbeat() {
    try {
      if (document.hidden) {
        return
      }
      const xhr = new XMLHttpRequest()
      xhr.open(
        'POST',
        'https://analytics.foodimpacts.org/ingress/6c31ff66-9b06-4e82-9443-64cfa5d404fc/script.js',
        true
      )
      xhr.setRequestHeader('Content-Type', 'application/json')
      xhr.send(
        JSON.stringify({
          idempotency: Shynet.idempotency,
          referrer: document.referrer,
          location: window.location.href,
          loadTime:
            window.performance.timing.domContentLoadedEventEnd -
            window.performance.timing.navigationStart
        })
      )
    } catch (e) { }
  },
  newPageLoad() {
    if (Shynet.heartbeatTaskId != null) {
      clearInterval(Shynet.heartbeatTaskId)
    }
    Shynet.idempotency = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
    Shynet.heartbeatTaskId = setInterval(Shynet.sendHeartbeat, parseInt('5000'))
    Shynet.sendHeartbeat()
  }
}

export default ({ app: { router }, isDev }) => {
  if (!router) {
    return
  }

  router.afterEach(() => {
    if (process.client && !isDev) {
      Shynet.newPageLoad()
    }
  })
}
