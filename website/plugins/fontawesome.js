import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faDownload,
  faEnvelope,
  faFileCode,
  faTimesCircle,
  faTools,
  faList
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Vue from 'vue'

library.add(faDownload)
library.add(faEnvelope)
library.add(faFileCode)
library.add(faTimesCircle)
library.add(faTools)
library.add(faList)

Vue.component('font-awesome-icon', FontAwesomeIcon)
