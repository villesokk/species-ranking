#!/bin/sh

set -e

cd $(dirname $(readlink -f $0))

hunspell -d en_GB $@
