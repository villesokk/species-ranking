Source code of [foodimpacts.org](https://foodimpacts.org/).

Requires [pandoc](https://pandoc.org/) to build the article and
[npm](https://www.npmjs.com/) for the website. The website is written
using the [NuxtJS](https://nuxtjs.org/) framework.

# Running

In the `website` subdirectory, run:

`$ npm i`

To run the development server:

`$ npm run dev`

Note that the article is also automatically rebuilt after changes when
using the development server.

# Deploying

In the `website` subdirectory:

`$ export NODE_ENV=production`

`$ npm run generate`

The bundled website is now in the `dist` subdirectory.

# Ranking logic

Ranking logic and data is in the `website/components/species.js` file.

# License

The favicon and other icons are designed by
[fontawesome.com](https://fontawesome.com) and are used under the terms of the
[Creative Commons Attribution 4.0
license](https://creativecommons.org/licenses/by/4.0/).

The software is licensed under the Expat (MIT) license which is available in the
LICENSE file.
